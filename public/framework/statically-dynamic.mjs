import { pageLoader } from "./pageLoader.mjs";

export class staticallyDynamic {
  constructor(staticDynamicOptions, showdownOptions, contentWrapper){
    this.pl = new pageLoader(staticDynamicOptions["contentBase"], showdownOptions);
    this.setTitle = staticDynamicOptions["setTitle"];
    this.prefixTitle = staticDynamicOptions["prefixTitle"];
    this.wrapping = contentWrapper;
  }
  view(page, anchor, dontState){
    anchor = anchor || "";
    let staticDynamicSelf = this;
    staticDynamicSelf.load(page)
        .then(pageContent => {
          staticallyDynamic.display(pageContent);
          staticDynamicSelf.attachTriggers();
          if(anchor !== ""){
            anchor = anchor.substring(1, Infinity);
            document.getElementById(anchor).scrollIntoView();
          }
          staticallyDynamicSelf.updateURL(page, anchor, dontState);
        });
  }
  display(pageContent){
    if(this.setTitle){
      document.title = this.prefixTitle + pageContent.metadata.title;
    }
    this.wrapping.innerHTML = pageContent.html;
  }
  attachTriggers(){
    let links = this.wrapping.querySelectorAll('a[href^="?"][href*="page="]');
    for(let i = 0; i < links.length; i++){
      links[i].addEventListener("click", (e) => {
        e.preventDefault();
        let url_string = e.target.href;
        let url = new URL(url_string);
        let dest = url.searchParams.get("page");
        let anchor = url.hash;
        console.log({dest, anchor});
        this.view(dest, anchor);
      });
    }
  }
  load(page){
    let staticDynamicSelf = this;
    let p = new Promise(function(resolve, reject) {
      staticDynamicSelf.pl.loadPage(page)
          .then(pageContent => resolve(pageContent) );
    });
    return p;
  }
  updateURL(pageUrl, hash, dontState){
    let url = new URL(window.location.href);
    let query_string = url.search;
    let search_params = new URLSearchParams(query_string); 
    // new value of "page" is set to the new url
    search_params.set('page', pageUrl);
    // change the search property of the main url
    url.search = search_params.toString();
    // set the url's hash.
    url.hash = hash;
    // the new url string
    let new_url = url.toString();
    if(dontState){
      window.history.replaceState({page:pageUrl}, null, new_url);
    } else{
      window.history.pushState({page:pageUrl}, null, new_url);
    }
  }
  
}
