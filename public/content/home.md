---
title: Statically Dynamic Home
some meta name: some meta content
---

# Home

This is Statically Dynamic, a framework for building single page, dynamic websites. Such as wikis, reference sites, help documentation, and more.

The content is loaded and processed on the client side and written in markdown with some YAML meta-data attached. This allows for easy modification of content without having to write lots of tedious HTML.

View a [test page](?page=test) with it's [Test Anchor](?page=test#checkmeout).
