import { staticallyDynamic } from "./statically-dynamic.mjs";
import { showdownOptions, sdOptions } from "./config.mjs";

let wrapper = document.getElementsByTagName("body")[0];

let sd = new staticallyDynamic(sdOptions, showdownOptions, wrapper);

sd.view("home")

//for debugging
window.sd = sd;
