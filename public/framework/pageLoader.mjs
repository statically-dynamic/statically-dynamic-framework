export class pageLoader {
  constructor(contentBase, showdownOptions){
    this.contentBase = contentBase;
    this.converter = new showdown.Converter(showdownOptions);
    this.preloaded = {};
  }
  loadPage(pageName){
    let pageLoaderSelf = this;
    let p;
    if(pageName in pageLoaderSelf.preloaded){
       p = new Promise((resolve, reject) => {
        resolve(pageLoaderSelf.preloaded[pageName]);
      });
    } else {
      p = new Promise((resolve, reject) => {
        let url = pageLoaderSelf.contentBase + pageName + ".md";
        fetch(url)
            .then(resp =>{
              if(resp.ok == false){
                throw "not ok.";
              }
              return resp.text();
            }).then(content => {
              let html = pageLoaderSelf.converter.makeHtml(content);
              let metadata = jsyaml.load(pageLoaderSelf.converter.getMetadata(true));
              pageLoaderSelf.preloaded[pageName] = {html, metadata};
              resolve(pageLoaderSelf.preloaded[pageName]);
            }).catch(err => {
              if(err == "not ok."){
                if(pageName == "404"){
                  console.warn("404 page returned 404!");
                }else{
                  console.log(pageName + " page returned 404.")
                  pageLoaderSelf.loadPage("404").then(pageContent => resolve(pageContent));
                }
              }
            });
      });
    }
    return p;
  }
}
