export let showdownOptions = {
  "customizedHeaderId": true,
  "metadata": true,
  "parseImgDimensions": true,
  "strikethrough": true,
  "tables": true,
  "ghCodeBlocks": true,
  "tasklists": true,
  "encodeEmails": true,
  "underline": true,
  "headerLevelStart": 1
};

export let sdOptions = {
  "contentBase": "./content/",
  "setTitle": true,
  "prefixTitle": ""
};
